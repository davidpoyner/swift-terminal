module.exports = (grunt)->
  options =
    xcode:
      app:
        options:
          project:'./testswift.xcodeproj'
          scheme:'testswift'
          arch:'i386'
          sdk:'iphonesimulator8.1'
          exportFormat:'APP'
          export:false
          clean:false
  
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-xcode'

  grunt.initConfig options

  grunt.registerTask 'default', ['xcode:app']
