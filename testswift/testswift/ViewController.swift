//
//  ViewController.swift
//  testswift
//
//  Created by David Poyner on 11/17/14.
//  Copyright (c) 2014 David Poyner. All rights reserved.
//

import UIKit
import SceneKit
import QuartzCore

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = PrimitiveScene( frame: self.view.frame )

        self.view.addSubview(scene.sceneView!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

