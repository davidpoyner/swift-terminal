//
//  PrimitiveScene.swift
//  testswift
//
//  Created by David Poyner on 11/18/14.
//  Copyright (c) 2014 David Poyner. All rights reserved.
//

import Foundation
import UIKit
import SceneKit
import SpriteKit
import QuartzCore

public class ParticleEmitter{
    // Private Methods
    var _emitter:SCNParticleSystem?
    var _numParticles:CGFloat?

    let fire:UIImage? = UIImage(named:"image_sequence_2x")

    var emitter:SCNParticleSystem { get { return self._emitter!} set (newVal) { self._emitter = newVal } }

    var numParticles:CGFloat {
        get{ return self._numParticles! }
        set (newVal){
            _emitter!.birthRate = newVal
            self._numParticles = newVal
        }
    }

    var position:(x:CGFloat?, y:CGFloat?)

    init(numParticles: CGFloat, position:(x:CGFloat?, y:CGFloat?)){
        self.emitter = SCNParticleSystem()
        self.numParticles = numParticles
        self.position = position

        setupEmitter()
    }

    func setupEmitter(){
        emitter.loops = true

        emitter.birthRate = self.numParticles
        emitter.emissionDuration = 2.0
        emitter.spreadingAngle = 25
        emitter.particleDiesOnCollision = true
        emitter.particleLifeSpan = 0.0025
        emitter.particleLifeSpanVariation = 0.83
        emitter.particleVelocity = 80
        emitter.particleVelocityVariation = 0.5
        emitter.particleSize = 1.5
        //emitter.stretchFactor = 0.15

        emitter.particleImage = fire
        emitter.imageSequenceRowCount = 4
        emitter.imageSequenceColumnCount = 4
        emitter.imageSequenceInitialFrameVariation = 16
        //emitter.acceleration = SCNVector3(x: 200.3, y:-300.2, z:0.2)
        emitter.dampingFactor = 1.84

        //animate()
        //emitter.affectedByGravity = true
    }
    func animate(view: SCNNode){
        let animation = CABasicAnimation(keyPath:"x")
        animation.fromValue = 0.0
        animation.toValue = 200.0
        animation.duration = 1.0
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        view.addAnimation( animation, forKey:"acceleration")

   }

    class Particle{
        init(){}
    }
}

public class PrimitiveScene{
    let BURST_RATE:Float = 0.3

    var sceneView:SCNView?
    var scene:SCNScene?
    var box = SCNBox(width: 15, height: 10, length: 12, chamferRadius: 0)
    var boxNode:SCNNode?
    var light:SCNLight?
    var lightNode:SCNNode?
    var cameraNode:SCNNode?
    var exp:SCNParticleSystem?
    var frame:CGRect?
    var pEmitter: ParticleEmitter?
    var physics: SCNPhysicsWorld?
    var floorNode: SCNNode?
    var timer:NSTimer?

    let floor = SCNFloor()

    init(frame: CGRect){
        self.frame = frame
        createParticleEmitter()

        setupScene()

        createBox()
        createFloor()
        createCamera()
        createLight()
        createPhyics()
    }

    internal func createFloor(){
        floor.reflectionFalloffEnd = 10
        floor.reflectivity = 0.8

        floorNode = SCNNode()
        floorNode?.geometry = floor
        floorNode?.position = SCNVector3(x:50, y:-40.0, z:0)

        let floorShape = SCNPhysicsShape(geometry: floor, options: nil)
        let floorBody = SCNPhysicsBody(type: .Static, shape: floorShape)

        floorNode?.physicsBody = floorBody

        scene?.rootNode.addChildNode(floorNode!)
    }

    internal func createBox( ){
        boxNode?.position = SCNVector3(x:50, y:0, z:2)
        boxNode?.rotation = SCNVector4(x:1, y:1, z:0.5, w:0.6)
        scene?.rootNode.addChildNode(boxNode!)

    }

    internal func createCamera( ){
        let cam = SCNCamera()        
        cam.focalDistance = 200.0
        //cam.focalSize = 2.0
        //cam.aperture = 0.025
        //cam.focalBlurRadius = 0.5

        cam.zFar = 1000.0

        cameraNode?.camera = cam
        cameraNode?.position = SCNVector3Make( 50, -30, 150)
        scene?.rootNode.addChildNode(cameraNode!)
    }

    internal func createLight(){
        light?.type = SCNLightTypeAmbient
        //light.color = UIColor.greenColor()
        light?.color = SKColor(white: 0.3, alpha: 1.0)
        let _spotLightParentNode = SCNNode()
        _spotLightParentNode.position = SCNVector3Make(50, 100, 90)
        
        let _spotLightNode = SCNNode()
        _spotLightNode.rotation = SCNVector4Make(1, 0, 0, CFloat(-M_PI_4))
        _spotLightNode.light = SCNLight()
        _spotLightNode.light?.type = SCNLightTypeSpot
        _spotLightNode.light?.color = SKColor(white: 1.0, alpha: 1.0)
        _spotLightNode.light?.castsShadow = true
        _spotLightNode.light?.shadowColor = SKColor(white: 0, alpha: 0.5)
        _spotLightNode.light?.zNear = 0
        _spotLightNode.light?.zFar = 1500
        _spotLightNode.light?.shadowRadius = 0.25
        _spotLightNode.light?.spotInnerAngle = 25
        _spotLightNode.light?.spotOuterAngle = 140

        lightNode?.light = light
        lightNode?.position = SCNVector3(x: 50, y: 10, z: 0)
    
        _spotLightParentNode.addChildNode(_spotLightNode)
        scene?.rootNode.addChildNode(_spotLightParentNode)
        scene?.rootNode.addChildNode(lightNode!)
   }

   internal func createParticleEmitter(){
        pEmitter = ParticleEmitter(numParticles: 6000, position:(x:3.0, y:4.0))
   }

    @objc internal func createRandomBlock(){
        var _w = randomCGFloat() * 10
        var _h = _w
        var _l = _w

        var b = SCNBox(width: _w, height: _h, length: _l, chamferRadius: 0)
        var bn = SCNNode(geometry: b)

        var _y = Float(randomCGFloat() * 100.0)
        var _x = Float(randomCGFloat() * 100.0)
        var _z = Float(randomCGFloat() * 50.0)

        var bs = SCNPhysicsShape(geometry:b, options:nil)
        var bb = SCNPhysicsBody(type: .Dynamic, shape:bs)

        bn.physicsBody = bb

        bn.position = SCNVector3(x:_x, y: _y, z:_z)
        scene?.rootNode.addChildNode(bn)
   }

   @objc func createPhyics(){
        return
        let max = 40
        var i = 0

        while i < max{
            createRandomBlock()
            i+=1
        }
        //let physicsBody = SCNPhysicsBody(edgeLoopFromRect: self.frame!)
        //self.scene.physicsBody = physicsBody
   }
   @objc func tick(){
        println("tick")
   }

   internal func setupScene(){
        sceneView = SCNView(frame: self.frame!)
        boxNode = SCNNode(geometry: box)

        scene = SCNScene()
        light = SCNLight()
        lightNode = SCNNode()
        cameraNode = SCNNode()
        exp = SCNParticleSystem()

        sceneView?.scene = scene!
        sceneView?.backgroundColor = UIColor.blackColor()

        sceneView?.autoenablesDefaultLighting = true;
        sceneView?.showsStatistics = true;
        sceneView?.allowsCameraControl = true;

        var emitter:SCNParticleSystem?

        let pNode = pEmitter!.emitter

        scene?.physicsWorld.gravity = SCNVector3(x:1.3, y:-40.9, z:0.0)
        scene?.addParticleSystem( pNode, withTransform: SCNMatrix4MakeRotation(0, 0, 0, 0))

        //pEmitter!.animate(pNode)
        timer = NSTimer.scheduledTimerWithTimeInterval( 0.3, target: self, selector: "createRandomBlock", userInfo: nil, repeats: true)
   }
    deinit {
        self.timer?.invalidate()
    }
    func randomCGFloat() -> CGFloat {
        return CGFloat(arc4random()) /  CGFloat(UInt32.max)
    }
}

